<?php

/**
 * Created by PhpStorm.
 * User: Madatyan
 * Date: 8/27/18
 * Time: 8:48 AM
 */
class Database
{
    protected $table;
    protected $query;
    protected $from;
    protected $columns;
    protected $joins;
    protected $orders;
    protected $bindings = array();

    public function __construct($host="localhost",$database="Comment",$username="root", $password = "")
    {
        $connection = new PDO("mysql:host=$host;dbname=$database", $username, $password);
        $this->connection = $connection;
    }

    /**
     * Compile an insert statement into SQL.
     *
     * @param  string  $table
     * @param array $values
     * @return int  $lastinsteredid
     */
    public function insert($table, $values)
    {
        $this->from = $table;
        $this->compileInsert($values);
        $this->execute($values);
        return $this->connection->lastInsertId();
    }

    /**
     * Compile an insert statement into SQL.
     *
     * @param  array  $values
     */
    public function compileInsert(array $values)
    {

        $keys = array_keys($values);
        $columns = implode(', ', $keys);

        // We need to build a list of parameter place-holders of values that are bound
        // to the query. Each insert should have the exact same amount of parameter
        // bindings so we will loop through the record and parameterize them all.
        $this->bindings = array_map([$this, 'parameter'], $keys);

        $parameters = implode(', ', $this->bindings);

        $this->query =  "insert into {$this->from} ($columns) VALUES ($parameters)";
    }

    public function parameter($item)
    {
        return ":".$item;
    }

    /**
     * Execute statement into SQL.
     *
     * @param  array  $values
     * @return  \PDOStatement $statement/boolean
     */
    public function execute($values=array())
    {
        try {
            $statement = $this->connection->prepare($this->query);
            $this->bindValues($statement, $values);
            $statement->execute();

            return $statement;
        }  catch( PDOException $e ) {
            return false;
        }
    }

    /**
     * Bind values to their parameters in the given statement.
     *
     * @param  \PDOStatement $statement
     * @param  array  $values
     * @return void
     */
    public function bindValues($statement, $values)
    {
        foreach ($this->bindings as $binding) {
            $key = ltrim($binding,':');
            $success = $statement->bindParam(
                $binding , $values[$key],
                is_int($values[$key]) ? PDO::PARAM_INT : PDO::PARAM_STR
            );
        }
    }

    /**
     * Set the table which the query is targeting.
     *
     * @param  string  $table
     * @return $this
     */
    public function from($table)
    {
        $this->from = $table;

        return $this;
    }

    /**
     * Set the columns to be selected.
     *
     * @param  array|mixed  $columns
     * @return $this
     */
    public function select($columns = ['*'])
    {
        $this->columns = is_array($columns) ? $columns : func_get_args();

        return $this;
    }

    /**
     * Add an "order by" clause to the query.
     *
     * @param  string  $column
     * @param  string  $direction
     * @return $this
     */
    public function orderBy($column, $direction = 'asc')
    {
        $this->orders[] = [
            'column' => $column,
            'direction' => strtolower($direction) == 'asc' ? 'asc' : 'desc',
        ];

        return $this;
    }

    /**
     * Add a join clause to the query.
     *
     * @param  string  $table
     * @param  string  $first
     * @param  string|null  $operator
     * @param  string|null  $second
     * @param  string  $type
     * @return $this
     */
    public function join($table, $first, $operator = null, $second = null, $type = 'inner')
    {
        $this->joins[] = compact(
            'type','table', 'first', 'operator', 'second'
        );

        return $this;
    }

    /**
     * Execute the query as a "select" statement.
     *
     * @param  array  $columns
     * @return PDORow $results
     */
    public function get($columns = ['*'])
    {
        $original = $this->columns;

        if (is_null($original)) {
            $this->columns = $columns;
        }

        $this->compileQuery();
        $statement = $this->execute();


        $results = $statement->fetchAll();

        return $results;
    }


    protected function compileQuery()
    {
        $this->query = "{$this->compileColumns()} {$this->compileFrom()} {$this->compileJoins()} {$this->compileOrders()}";
    }

    /**
     * Compile the "select *" portion of the query.
     *
     * @return string
     */
    protected function compileColumns()
    {

        return 'SELECT '. implode(',', $this->columns);
    }

    /**
     * Compile the "from" portion of the query.
     *
     * @return string
     */
    protected function compileFrom()
    {
        return 'FROM '.$this->from;
    }

    /**
     * Compile the "join" portions of the query.
     *
     * @return string
     */
    protected function compileJoins()
    {
        $joins = array();
        foreach ($this->joins as $join) {
            $joins[] = "{$join['type']} join {$join['table']} on {$join['first']}{$join['operator']}{$join['second']}";
        }

        return implode(" ",$joins);
    }

    /**
     * Compile the "order by" portions of the query.
     *
     * @return string
     */
    protected function compileOrders()
    {
        if (! empty($this->orders)) {
            $orders = array();
            foreach ($this->orders as $order){
                $orders[] = "{$order['column']} {$order['direction']}";
            }
            return 'order by '.implode(', ', $orders);
        }

        return '';
    }
}