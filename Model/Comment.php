<?php
/**
 * Created by PhpStorm.
 * User: Madatyan
 * Date: 8/27/18
 * Time: 9:16 AM
 */

require_once "Database.php";

class Comment
{

    protected $db;
    public $insertId;

    /**
     * create a new instance of database.
     *
     */
    public function __construct()
    {
        $this->db = new Database();
    }

    /**
     * Compile an insert statement into SQL.
     *
     * @param  string  $comment
     * @param string $name
     * @return bool
     */
    public function addComments($comment,$name)
    {
        try {
            $this->insertId = $this->db->insert('comments',array('comment'=>$comment,'name'=>$name));
        } catch(PDOException $e) {
            return false;
        }

        return true;
    }

    /**
     * Compile an insert statement into SQL.
     *
     * @param  int  $parentId
     * @return boolean
     */
    public function insertChildRelationship($parentId)
    {
        try {
            $this->db->insert('parent_child_comments',array('parentId'=>$parentId,'childId'=>$this->insertId));
        } catch(PDOException $e) {
            return false;
        }

        return true;
    }

    /**
     * Compile an insert statement into SQL.
     *
     * @return array  $comments
     */
    public function getAllComments()
    {
        $comments = $this->db->from('comments')->select('*', 'DATE_FORMAT(created, "%c/%d/%Y") as date')
                ->join('parent_child_comments','comments.id','=','parent_child_comments.childId','left')
                ->orderBy('coalesce(id,childId)')->get();

        return $comments;
    }
}