/**
 * Created by Madatyan on 8/26/18.
 */
var websocket = null;

$(document).ready(function()
{

    init();

    $('#comment_form').on('submit', function (event) {
        event.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            url:"addComment",
            method:"POST",
            data:form_data,
            dataType:"JSON",
            success:function(data)
            {
                if(data.success == true)
                {
                    $('#comment_form')[0].reset();
                    $('#comment_id').val('0');
                    websocket.send(JSON.stringify(data));
                    addNewComment(data);
                } else {
                    $('#comment_message').html('There was a problem with submitting your comment. Please try again.');
                }
            }
        })
    });

    $(document).on('click', '.reply', function(){
        var comment_id = $(this).attr("id");
        $('#comment_id').val(comment_id);
        $('#name').focus();
    });

});

function init() {
    if ("WebSocket" in window) {
        websocket = new WebSocket('ws://localhost:8080');
        websocket.onopen = function(data) {
            console.log("Connection established!");
        };

        websocket.onmessage = function(e) {
            console.log(e.data);
            var data = JSON.parse(e.data);
            addNewComment(data);
        };

        websocket.onerror = function(e) {
            cleanUp();
        };

        websocket.onclose = function(data) {
            cleanUp();

        };
    } else {
        alert("Websockets not supported");
    }

    load_comment();
}

function cleanUp()
{
    userName = null;
    websocket = null;
}

function load_comment()
{
    $.ajax({
        url:"getComments",
        method:"POST",
        dataType:"JSON",
        success:function(data)
        {
            var html = '';
            for(i=0; i<data.length; i++){
                addNewComment(data[i]);

            }
        }
    })
}

function setupHtml(id, name, comment, parentId, date, level, marginLeft)
{
    var html = '<div class="panel panel-default" style="margin-left:' + marginLeft + 'px">' +
        '<div class="panel-heading">By <b>' + name + '</b> on <i>' + date + '</i></div>' +
        '<div class="panel-body">' + comment ;
    if(level < 3) {
        html += '<button type="button" class="btn btn-default reply" id="' + id +'">Reply</button>';
    }

    html += '</div></div>';

    return html;

}

function appentTo(parentId, html)
{
    if(parentId){
        $('#'+parentId).closest('.panel-default').append(html);
    } else {
        $('#display_comment').append(html);
    }
}

function addNewComment(data)
{
    let parentId = parseInt(data.parentId);
    let marginLeft = getMargin(parentId);
    let level = getLevel(parentId);
    var html = setupHtml(data.id, data.name, data.comment, parentId, data.date, level, marginLeft);

    appentTo(parseInt(parentId), html);
}

function getMargin(parentId)
{
    let marginLeft = 0;
    if (parentId) {
        marginLeft = 40;
    }

    return marginLeft;
}

function getLevel(parentId)
{
    let level = 0;
    if (parentId) {
        level = $( '#'+parentId ).parents('.panel-default').length +1;
    }

    return level;
}