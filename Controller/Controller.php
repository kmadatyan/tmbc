<?php

/**
 * Created by PhpStorm.
 * User: Madatyan
 * Date: 8/26/18
 * Time: 4:52 PM
 */
require_once "Model/Comment.php";


class Controller
{

    protected $comment = null;

    public function __construct()
    {
        $this->comment = new Comment();
    }

    public function addComment()
    {
        $data['name'] = $_POST["name"];
        $data['comment'] = $_POST["comment"];
        $data['parentId'] = $_POST["comment_id"];
        $data['date'] = date('m/d/Y');
        $data['success'] = $this->comment->addComments($data['comment'], $data['name']);
        if ($data['success']) {
           $data['id'] = $this->comment->insertId;
        }

        if ($data['parentId']) {
            $data['success'] = $this->comment->insertChildRelationship($data['parentId']);
        }

        echo json_encode($data);
    }

    public function getComments()
    {
        echo json_encode($this->comment->getAllComments());
    }
}