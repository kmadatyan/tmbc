<?php
/**
 * Created by PhpStorm.
 * User: Madatyan
 * Date: 8/26/18
 * Time: 11:21 PM
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Comment System using PHP and Ajax</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <style>
        .reply {
            color: green;
            font-size: 14px;
        }
    </style>
</head>
<body>
<br />
<h2 align="center"><a href="#">Comment System using PHP, Ajax, Websocket</a></h2>
<br />
<div class="container">
    <form method="POST" id="comment_form">
        <div class="form-group">
            <input type="text" name="name" id="name" required class="form-control" placeholder="Enter Name" />
        </div>
        <div class="form-group">
            <textarea name="comment" required id="comment" class="form-control" placeholder="Enter Comment" rows="5"></textarea>
        </div>
        <div class="form-group">
            <input type="hidden" name="comment_id" id="comment_id" value="0" />
            <input type="submit" name="submit" id="submit" class="btn btn-info" value="Submit" />
        </div>
    </form>
    <span id="comment_message"></span>
    <br />
    <div id="display_comment"></div>
</div>
</body>
</html>
<script src="js/main.js"></script>