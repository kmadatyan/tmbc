<?php
/**
 * Created by PhpStorm.
 * User: Madatyan
 * Date: 8/26/18
 * Time: 11:10 PM
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$uri = getCurrentUri();
if(!$uri) {
    include_once "View/comment.php";
} else if(file_exists('View/'.$uri)) {
    include_once "View/".$uri;
} else {
    require_once "Controller/Controller.php";
    call_user_func_array(array(new Controller(), $uri),  array());
}

/**
 * Define the current relative URI.
 *
 * @return string
 */
function getCurrentUri() : string
{
    // Get the current Request URI and remove rewrite base path from it (= allows one to run the router in a sub folder)
    $uri = $_SERVER['REQUEST_URI'];
    // Don't take query params into account on the URL
    if (strstr($uri, '?')) {
        $uri = substr($uri, 0, strpos($uri, '?'));
    }
    // Remove trailing slash + enforce a slash at the start
    return trim($uri, '/');
}

?>
