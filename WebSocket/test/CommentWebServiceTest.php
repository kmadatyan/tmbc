<?php

/**
 * Created by PhpStorm.
 * User: Madatyan
 * Date: 8/27/18
 * Time: 12:08 PM
 */

namespace WebSocket;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;


class CommentWebServiceTest extends \PHPUnit_Framework_TestCase
{
    protected $server;

    protected $app;

    protected $port = 8080;



    public function setUp() {
        $this->app = $this->getMock('\\Ratchet\\MessageComponentInterface');
        $comment = new CommentWebService();

        $this->server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    $comment
                )
            ),
            $this->port
        );
    }

    public function testOnData() {
        $msg = 'Hello World!';

        $this->app->expects($this->once())->method('onMessage')->with(
            $this->isInstanceOf('\\Ratchet\\ConnectionInterface')
            , $msg
        );

        $client = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_option($client, SOL_SOCKET, SO_REUSEADDR, 1);
        socket_set_option($client, SOL_SOCKET, SO_SNDBUF, 4096);
        socket_set_block($client);
        socket_connect($client, 'localhost', $this->port);
        socket_write($client, $msg);
        socket_shutdown($client, 1);
        socket_shutdown($client, 0);
        socket_close($client);
    }

}
