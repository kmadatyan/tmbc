<?php
/**
 * Created by PhpStorm.
 * User: Madatyan
 * Date: 8/26/18
 * Time: 12:56 AM
 */



namespace WebSocket;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once "CommentWebService.php";

$comment = new CommentWebService();

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            $comment
        )
    ),
    8080
);

$server->run();
