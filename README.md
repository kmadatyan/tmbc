I wanted to show how I could create a new class and websocket. I would use lumens for my webserver and website but I wanted to show my own work. The websocket uses ratchet but the rest is my work. I created somewhat a router in index.php that looks for controller class and its method.

I have all my modules Model/View/Controller in each folder. 

Setting Up Database

Run comment.sql which will create a new Database called Comment and 2 tables comments and parent_child_comments. The comments table will only have the comment user added. The parent_child_comments is the relational table that attaches the parent comment to child.

To Connect to your local database to do the inserting and fetching then please make sure the configuration for a new Database on line 23 has the correct database hostname, username, and password

Setting Up the WebSocket Server

Open a terminal. Please make sure php is in your global path. If it is not then please go to where php is and Run the command "php {DIRECTORY}/WebSocket/startWebSocket.php" and if it is in your global path then go the the Websocket directory from the clones repository and run the command "php WebSocket/startWebSocket.php" . This will start the websocket. This is for other people who are already on the page to get updates without Refreshing

Setting Up the Website

Setup a website configuration in apache/nginx. Open 2 browsers pointing to that domain.

Seeing Websocket, Ajax, and PHP Work Together

Now type a comment in 1 browser and see it add to the DOM. Now Go to the 2nd browser window. You should see the comment there as well. You should also see it get added to the Database